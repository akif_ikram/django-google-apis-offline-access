Using Google APIs with Django
=============================

A sample project that shows how to work with Google APIs through Django.

# To use:

1. Download the project into a new directory
2. Go the the main directory and run: `python manage.py runserver`
3. Then go to your web browser, and enter the URL: `http://localhost:8000/google_auth/`


## Other Information
* I get the profile information from the authorized response, so that the
user can choose whatever account (assuming they have multiple), and log in.
The code picks their information from what Google returns.

* The client_id and client_secret for the application are stored in a file in
json format. They are used to identify the app to Google (it shows the Information
you enter when you create the client id).

* As the user gives authorization, they get their own file to store their credentials.
I have a user identifier in the filename on lines 34, 97, such as 'creds_1', or
'creds_akif'.  This keeps users' authorization status separate from other users.

* In the app, I use "is_google_authorized()" to ensure I can make the
actual service call.  That procedure makes sure that the token is valid, and
refreshes the token if it has expired.

## Issues?
Let me know!
