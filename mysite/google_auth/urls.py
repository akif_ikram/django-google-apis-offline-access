from django.urls import path
from . import views

app_name = 'google_auth'
urlpatterns = [
        path('', views.IndexView, name='index'),
        path('ga/', views.AuthorizeGoogle, name='google_auth'),
]
