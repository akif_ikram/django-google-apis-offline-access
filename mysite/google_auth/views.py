import pytz
UTC = pytz.timezone('UTC')

import json
import httplib2
import urllib

from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import reverse, redirect, render
from django.http import HttpResponse
from django.utils import timezone

# Google Auth
from googleapiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import OAuth2WebServerFlow, OAuth2Credentials, AccessTokenCredentials, AccessTokenRefreshError

def IndexView(request):
    return HttpResponse (
"""
<p>The core code to use Google (Calendar) Apis with Django.</p>

<p>I get the profile information from the authorized request also, so that the
user can choose whatever account (assuming they have multiple), and log in.
The code picks their information from the authorized account.</p>

<p>The client_id and client_secret for the application are stored in a file in
json format. Those are used to identify to Google who we are.  Then as the user
gives authorization, each user gets their own file to store their credentials.</p>

<p>I have a user identified in the filename on lines 34, 97.  This keeps
users' authorization status sorted out.</p>

<p>In the rest of the app, I use "is_google_authorized()" to ensure I can make the
actual service call.  That procedure makes sure that the token is valid, and
refreshes the token if it's not.</p>
"""
)

# Call this before using Google Service
def is_google_authorized():
    storage = Storage(
                    'a_filename'
                )

    credentials = storage.get()
    if not credentials or credentials.invalid:
        return (False, None)
    else:
        try:
            # Connect to google using stored credentials
            if UTC.localize(credentials.token_expiry) <= timezone.now():
                http = httplib2.Http()
                http = credentials.refresh(http)
                storage.put(credentials)

            service = build('calendar', 'v3', credentials=credentials)
            return (True, service)
        except AccessTokenRefreshError:
            return (False, None)


def AuthorizeGoogle (request):
    f = open ('file_containing_application_id_and_secret(from Google).json','r')
    app_creds = json.loads(f.read())
    f.close()

    context = {}

    flow = OAuth2WebServerFlow (
                    client_id=app_creds['client_id'],
                    client_secret=app_creds['client_secret'],
                    scope=['profile','email','https://www.googleapis.com/auth/calendar'],
                    access_type='offline',
                    prompt='consent',
                    redirect_uri=request.build_absolute_uri(
                        reverse('google_auth:google_auth')
                        ),
                )
    http = httplib2.Http()

    error_code =  request.GET.get('error')
    success_code = request.GET.get('code')

    # Returned from Google Authotization request
    #  -- with error
    if error_code:
        return HttpResponse ('Google returned: ' + error_code )
    #  -- successfully
    elif success_code:
        credentials = flow.step2_exchange(success_code)
        user_information = {
                'id': credentials.id_token['sub'],
                'name': credentials.id_token['name'],
                'first_name': credentials.id_token['given_name'],
                'last_name': credentials.id_token['family_name'],
                'email': credentials.id_token['email'],
                'image_url': credentials.id_token['picture'],
            }

        # Do what you have to do..., my app creates the calendar, and logs
        # the person in
#        user, cal_id =  process_auth (request, user_information)

        # If multiple users, add their user id in the file name
        storage = Storage( 'a_filename' )
        storage.put(credentials)

        return redirect(reverse('google_auth:index'))

    # Called directly
    else:
        auth_uri = flow.step1_get_authorize_url()
        return redirect(auth_uri)

    try:
        http = credentials.authorize(http)

        # All is good!!!

        return redirect(reverse('google_auth:index'))
    except AccessTokenRefreshError:
        return redirect(reverse('google_auth:google_auth'))
